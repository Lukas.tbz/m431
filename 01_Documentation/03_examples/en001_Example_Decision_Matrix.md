# Handout - Decision matrix

This is an actual example that was used to select a new datacenter provider. As you can see it is several smaller decision matrices feeding into an overview.

### This is how it was done:
* Before issuing our request we determined the evaluation criteria and applied weighting to each one.
* The criteria had the same names as the chapters in the proposal.
* Once all the proposals were recieved each team member read each proposal (each several hundred pages) and filled in the scores on the matrix accordingly.
* We then met as a team discussed each member's scores before agreeing on & applying a final score 
* Note that price was not part of the evaluation matrix


![General Weighting][weighting]

![Company and Team][category]

![The result][overview]

[overview]: ../../01_Documentation/04_resources/images/03_en001_Overview.jpg
[weighting]: ../../01_Documentation/04_resources/images/03_en001_weightings.jpg
[category]: ../../01_Documentation/04_resources/images/03_en001_Company_and_Team.jpg
