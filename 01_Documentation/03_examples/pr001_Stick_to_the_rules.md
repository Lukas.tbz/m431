# Example - Abide by the rules
Below is an extract from a "Request for Proposal" (RfP). It asked Infrastructure providers to give estimates to provide srvices & host applications in their datacenters. In this section it shows what will happen if you do not abide by the simple rules regarding timelines and templates.  

A simple disregard for these rules could have ended up with:
* The Infrastructure provider missing out on a contract that was worth millions. 

![Extract from RfP][extract]

[extract]: ../../01_Documentation/04_resources/images/03_pr001_stick_to_the_rules.jpg
