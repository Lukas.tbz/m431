# Example - Goals
Below is an extract from a "Request for Proposal" (RfP). It asked Infrastructure providers to give estimates to provide services & host applications in their datacenters. In this section it outlines the goals that they wish to achieve with this project. 

![Extract from RfP][goals]

[goals]: ../../01_Documentation/04_resources/images/03_pr002_goals.jpg
