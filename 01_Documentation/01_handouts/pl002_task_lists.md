# Handout - Task lists

## Introduction
A tasklist is a list of items that need to be performed. Some tipps:
* Keep each task as small and granular as possible
* Keep the number of deliverables as low as possible
* Avoid bundling/cramming several items into one task
* Tasklists must be managed and kept up to date!

### It should include
1. Description of the task
2. Who raised the task
3. Who is supposed to perform the task
4. When is the task due to be finished

## Tools 
### Excel (or similar)
* **Advantage**: simple to use and flexible
* **Disadvantage**: There is that risk that you end up with several different copies of the task list. There is no history of the activities

### Jira/Gitlab (or similar)
* **Advantage**: Central and has all the fields you need for a task list. Tasks are numbered
* **Disadvantage**: Sometimes it is not possible to customize the fields to suit your specific needs

## Example

