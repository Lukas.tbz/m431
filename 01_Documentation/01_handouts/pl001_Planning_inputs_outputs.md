# Handout - IPERKA - Planning

## Inputs
* Detailed descriptions of each deliverable
* Requirements list
* Dates & times
* Quality expectations

## Activities
* Resource planning [note 1], [Exercise 1]
* Breakdown analysis [note 2], [note 3], [Exercise 2]
* Effort estimation
* Identify sequences
* Scheduling [note 4]

## Control
* Are you milestones in line with those you identified of the Information gathering?
* Review the plans with the team
* Present the plans to whomever gave you the assignment
* Is the workload distributed evenly?
* Do you have a buffer for unplanned delays or absences?

## Outputs
* Resource plan (plan who is needed & when, who is away)  
* Task lists
* Work schedule (when will each task be performed)

## Block diagram
![Block diagram of a process](../../01_Documentation/04_resources/images/01_pl001_inputs_outputs.jpg)

[note 1]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/pl004_resource_plan.md
[note 2]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/pl005_work_breakdown.md
[note 3]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/pl002_task_lists.md
[note 4]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/pl006_task_scheduling.md

[Exercise 1]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/02_exercises/pl001_Create_a_resource_plan.md
[Exercise 2]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/02_exercises/pl002_Identify_the_tasks.md