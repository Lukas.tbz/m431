# Handout - Project Lifecycle

## Generic Project Lifecycle
In the diagram you can see the typical phases of a project (geneic).

### Phases
* Initiation
* Planning
* Execution
* Monitor & Control
* Closure

![Block diagram of a process][process]


[process]: ../../01_Documentation/04_resources/images/01_pr002_Project_Lifecycle.jpg
