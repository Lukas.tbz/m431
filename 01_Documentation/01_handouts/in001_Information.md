# Handout - Information

Getting a task is similar to being handed a single piece of a jigsaw puzzle and being told to: "_place it in the middle so that all the other peices will easily fit in later_". Think if the questions you would need to ask in order to get this right.

## Questions you need to ask
Here are some questions (question categories) that you should consider asking

### General Questions
* Does something similar already exist?
* Has it been done before?

### Questions about people/teams
* Who is requesting this task?
* Who is/are the end-user(s)?
* Who will test it?
* Who will sign-off the task once it is complete?

### Technical Questions
* Here you need to find out what exactly all the technical details of how your _deliverable(s)_ should look like & perform.

### Dates & Times
* When is it due?
* When should I start?

### Questions about Context
* It it a once-off piece of code/task?
* Is it something that is going to last for the next X years?
* What is the goal of this task?
* How does this task fit in to the project?
* Are there any dependencies to other tasks or teams?

### Questions about Quality
* Cheap & nasty or Bullet-proof?
* Who will test it?
* How can it be tested?

### Questions about Security
* What type of data is involved?
* Is it sensitive data?

