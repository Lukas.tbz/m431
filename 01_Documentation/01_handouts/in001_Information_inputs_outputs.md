# Handout - IPERKA - Inform

## Inputs
* Ideas
* Goals [See note 1]

## Activities
* Interviews [See note 2] and [See note 3]
* Questionaires 
* Internet research [See note 5]and [See note 6]
* etc

## Control
* Are the goals still valid as more information becomes known?
* Has the requestor confirmed that your information & requirments are correct?

## Outputs
* Detailed descriptions of each deliverable
* Requirements list [See note 4]
* Dates & times
* Quality expectations

## Block diagram
![Block diagram of a process](../../01_Documentation/04_resources/images/01_in001_inputs_outputs.jpg)


[See note 1]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/pr003_Goals_and_Objectives.md
[See note 2]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/in004_Open_and_Closed_questions.md
[See note 3]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/in001_Information.md
[See note 4]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/in002_Requirements.md
[See note 5]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/in005_Secure_browsing.md
[See note 6]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/in006_Sources.md
