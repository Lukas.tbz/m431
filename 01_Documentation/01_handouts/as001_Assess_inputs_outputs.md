# Handout - IPERKA - Assess / Auswereten

## Inputs
* Plans
  * tasklists
  * timeplan / taskschedule
* Requirements
* Product

## Activities
* Gather facts:
  * What went wrong
    * how often
    * how long
    * impact of it going wrong
* Compare your planned progress against actual progress 
* Review successes:
  * Understand why they were a success?
* Review issues encountered:
  * Could they have been prevented?
  * Did you react properly?
  * What can be done to prevent such issues in the future?
* Ask yourselves:
  * If you were to start again would you do differently?
  * Identify areas/items that you can improve
* Be constructive:
  * Do not fingerpoint
  * Use the we form: "_What can **we** do to prevent it from happening again_" 

## Outputs
* Decisions
* Knowledge

