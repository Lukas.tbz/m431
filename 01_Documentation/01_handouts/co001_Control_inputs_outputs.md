# Handout - IPERKA - Control

## Inputs
* Testplans
* Requirements
* Product
* Documents

## Activities
* Write testcases
* Create test data
* Execute tests
* Code reviews
* Document reviews
* Walk throughs
* Demos

## Control
* Are the testcases in-line with the Assignment Goals?
* Do the testcases cover the requirements?
* Are the testcases in-line with Quality expectations?

## Outputs
* Testcases [See note 1]
* Test Protocol [See note 2]
* Defect Lists (findings)

## Block diagram
![Block diagram of a process](../../01_Documentation/04_resources/images/01_co001_inputs_outputs.jpg)

[See note 1]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/co002_testcases.md
[See note 2]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/co003_testprotocol.md
