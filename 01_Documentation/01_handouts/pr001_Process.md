# Handout - A process

## Definition
"_A process is a structured set of activities designed to accomplish a specific objective. A process takes one or more defined inputs and turns them into defined outputs._".  
**Ref:** ITIL Service Design, page 20, ISBN 978-0-11-331305-1

## Block diagram
![Block diagram of a process](../../01_Documentation/04_resources/images/01_pr001_Process.jpg)

## Example
We would like to bake a cake, the outcome of this process could be: a cake to serve as a dessert to tonight's dinner.

### Inputs
* Flour
* Butter
* Eggs
* Oven etc

### Structured activities
* mix the flour, eggs and butter
* preheat the oven to 180C
* etc

### Control
* Weighing scales
* The temperature guage on the oven
* Your sense of taste (too sweet?)
* Your sense of sight: does it look dry? does it look nice & golden? or do I see flames?
* Your sense of smell: does it smell of butter (underbaked), do I smell burning?

### Output
* A cake
