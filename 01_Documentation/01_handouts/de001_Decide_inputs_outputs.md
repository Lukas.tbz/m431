# Handout - IPERKA - Decide

## Inputs
* Items that need to be decided upon such as:
  * Which assignment topic to choose
  * Which tools to use
  * Build your own tool or use something on the Internet

## Activities
* Evaluation [See note]
* Estimation
* SWOT analysis

## Control
* Will your decision impact the assignment goals?
* Will your decision impact the work plan/milestones etc?
* Will your decision impact your resource plan?

## Outputs
* Decision
* Decision log (who decided & when)
* Decision document (that documents how you came to this decision)

## Block diagram
![Block diagram of a process](../../01_Documentation/04_resources/images/01_en001_inputs_outputs.jpg)

[See note]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/de002_Decision_matrix.md
