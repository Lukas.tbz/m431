# Handout - Work-breakdown

## Introduction
* In order to create a workplan or a task list we need to take a deep dive into the assignment and breakdown each area/task into smaller units of work. The reason for doing this is:
  * Get a better understanding of the work to be performed
  * To create small units of work that can be assigned to each team member 

## How to do it
* Using the scope and requirements of your assignment identify the high the high level tasks (E.g write code, write documentation, setup server) 
* For each high level main task that you identify take a deeper dive:
  * What do you need to prepare/set up before starting the task?
  * What do you need to perform the main task? 
  * Can you split the main task further? - Use the requirements & features as a guide
  * What do you need to test the task?

## The result
* A list of tasks covering the whole assignment that need to be perfomed. Each task is: 
  * Small enough to be worked on by one person
  * Has a specific outcome

## Next steps
* Now that you have a list of tasks you will need to [schedule them]

## Example - write code
* In this example you will write a program with two main functions, it is part of a larger project. The work-breakdown would look something like this
  * Download & install developlemt tools
  * Create git repository
  * Write flowchart / state diagram etc
  * Review diagram & make corrections
  * For each function you ifentified
    * Create Unit testcases 
    * Code function
    * Unit test function

## Example - write documentation
* In this example you will write the supporting document for your project.
  * Identify chapters & sub-chapters
  * Define directory structure in git
  * For each chapter 
    * Gather supporting content (screenshots, tables etc)
    * Write chapter
    * Review for correctness 
    * Review for spelling grammer etc 
  * Joint review of whole document
  * Update document
  * Independant review of document
  * Update document
  
[schedule them]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/pl006_task_scheduling.md


