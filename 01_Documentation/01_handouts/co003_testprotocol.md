# Handout - Testprotocol

## Introduction
This document is provided to your Requestor to show what you have tested and what the outcome was. The goal is to improve transparancy and display to the Requestor that their quality expectations have been met.

### The testprotocol should include
1. A list of testcases that were executed:
   * Testcase name
   * The number of times it was executed
   * The final State (PASS / FAILED)
2. The number of defects (bugs) that were found (and fixed)
3. The number of defects (bugs) that are not yet fixed


### Examples
![Testcase for a tasklist](../../01_Documentation/04_resources/images/01_co003_testprotocol_game.jpg)
