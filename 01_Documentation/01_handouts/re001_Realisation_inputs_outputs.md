# Handout - IPERKA - Realisation

## Inputs
* Workplans
* Task lists
* Resource plans

## Activities
* Coding
* Staging
* Deploying
* Unit testing
* Documentation

## Control
* Are you able to keep to the schedule in the workplan?
* Are all tasks assigned & in-progress?
* Does the product still conform to the requirements?
* Does the product still meet the goals that were defined?
* Have all issues encountered during the implementation been correctly identified & prioritised?

## Outputs
* Product(s)
  * A website, a game etc.
  * Installation instructions
  * User guides
* Documentation
  * Updated task lists
  * Comments made in code & commit statements etc
  * Issue logs for any issues that were encountered during implementation
  * A list of known (unresolved) issues

## Block diagram
![Block diagram of a process](../../01_Documentation/04_resources/images/01_re001_inputs_outputs.jpg)
