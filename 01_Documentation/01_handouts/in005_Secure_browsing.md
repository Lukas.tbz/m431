# Handout - Secure browsing
* While gathering information & infoming yourself about you assignment so too is the Internet gathering information about you

## Some tipps 
* Always set your browser to private or incognito browsing mode
* If you want more more privacy then consider using a more private browser such as Brave or even Tor
* Use an alternative search engine such as DuckDuckGo
* Consider disabling javascript on your browser
* Ensure that you have disabled Tracking Cookies
* Disable autofilling of usernames, addresses etc.
* Use a VPN (Virtual private network) if possible
* Get out of the habit of pressing "Allow all" when prompted to accept cookies, select essential cookies only
* Perform browser hygene regularly such as:
  * Delete browser cache, data & cookies
  * Delete downloads
  * Review website & data permissions
  * Review your security settings
 
## Fingerprint checking tools
Just like a real fingerprint your PC can be easily and uniquely identified, so it too can be traced accross website. The sites below will give you an indication of just how much data they can get from your PC and just how identifiable it is.
* https://www.deviceinfo.me
* https://coveryourtracks.eff.org/ 
* https://amiunique.org
* https://privacy.net

