# Internet sources
* https://scholar.google.com
* https://www.duckduckgo.com

## Tutorials
* https://www.tutorialspoint.com/

## On-line courses (some of which are free)
* https://www.udemy.com/
* https://www.coursera.org/
* https://www.edx.org/

## Troubleshooting
* https:///www.stackoverflow.com

# System sources (Linux)
* The _man_ page, just type _man_ and the command you are interested in: E.g. _man find_
* The quick help, just type the command followed by --help E.g. _find --help_

# Artificial Intelligence
## Introduction
AI has been around since the 1980's. It's main impediments were: hardware, data gathering and network. None the less the researchers knew what they wanted and still persevered. The advent of the Internet search engines and data lakes removed the data restrictions as too did developments in hardware both software. AI uses a lot of math's in particular statics and probability calculations.
### How does AI work 
* You develop a set of algorithms to solve a particular issue, for example to diagnose an illness  
* You then 'train your system' by feeding it information - in our case we 'feed' it medical journals and text books used to teach doctors. The testers then start providing symptoms and grade the AI's answers, AI uses this feedback to improve the quality of the next answers
* When the AI is launched to the public AI learns from the user by asking it to 'correct' or grade it suggestions
* After a period of time it is not uncommon to have to retrain AI so that it is aware of new data facts - just like a normal IT person needs to go for training

### Example
An anti fraud system in a bank
* They program an algorithm to detect fraudulent transactions
* They then use past examples of fraud to train the system
* When it is in use:
 * The Bank operator will confirm all the transactions the AI systems finds - i.e. they tell AI that it is correct
 * The Bank operator will also mark the fraudulent transaction that the AI missed - now the AI knows of a new fraud pattern
* Bad actors will also use their evil AI to teach the bank's good AI new but bad habits
* The bank's IT team have to periodically check and retrain their AI 

## When using AI 
* Remember, you are learning from AI and AI is asking you if the answer is correct so just who is learning from whom here?
* AI has been known to intentionally provide bad answers i.e. it is testing you
* AI is not a definitive source - there are no peer reviews there is nobody to qualify the answer  

# Some tipps 
* Always understand the answer
  * If is a syntax issue confirm it by consulting another site
  * If it is a description of how you should perform something, then write down the answer but in your own words
* There are two approaches to gathering information to solve a particular issue 
  * Studying, reading, understanding, or
  * Brute force: simply googling for an answer and then trying and trying until you finally find an answer that works without really knowing why it worked
* When looking for a solution to a particular problem, review the answers carefully. Quality of the answers can vary and it is not uncommon for someone to provide advice who has a less understanding than you
* 
# Sources
* You may quote a source **but** you __must__ enclose it in quotation marks and use an _itallic_ font
* You may use an image **but** you __must__ name the URL (undreneath the image) 
 
