# Handout - IPERKA - Decision matrix
## Introduction
This describes how to approach creating an decision matrix to select between a number of items. *NOTE*: the example here is for choosing between two laptops, it could also be applied to a non technical topic such as choosing between two subject to do an assignment on. 

## Step 1 - reduce the number of items to chose between down to a manageable number
* Reduce the number of items to chose between down to a manageable number (E.g. no more than 4 items). This selection can be made by applying a simple criteria such as price, availability etc. The goal should be that the remaining items should be reasonably similar to each other.

## Step 2 - Define a Decision matrix
* Identify 4 or 5 criteria that are important to you or your asignment.
* For each criteria you identified give it a weighting in percent such that the sum of the weightings is 100%.
* Define a scoring system whereby 0 is criteria not met and the top mark is criteria exceeded. For your scoring a range of 0 to 3 would be sufficient, anything higher than 3 ise harder to grade. Example:
  * 0 = critera not present
  * 1 = criteria partially met
  * 2 = criteria satisfied 
  * 3 = criteria exceeed
* In MS Excel (or similar) create a matrix.
* In some cases it may be usefull to define 2 or 3 levels to allow you to define & score sub-criteria.

## Step 3 - Each team member applies the score
* Each team member scores their copy of the matrix.
* The team then meets to discuss and agree on the scores.
* The agreed scores are applied to a centryl copy of the matrix.
* It is always possible that although the matrix says that Product A is the one to chose, the team still selects Product B. In such cases it is important to document your motivation for selecting the product.

## Example of a Decision matrix for 2 laptops
* Level 1 has:
  * Display 30%
  * CPU 20%
  * Disk 20%
  * Other 10%
* Level 2 has sub-criteria, for eaxmple in the case of the Display there are sub-criteria Size and Resolution.

![Example](../../01_Documentation/04_resources/images/01_en002_evaluation_matrix.jpg)
