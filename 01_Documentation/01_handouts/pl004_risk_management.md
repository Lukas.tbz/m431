# Handout - Risk Management

## Introduction
So now you have gathered your information, created a plan, made all your decisions. So you may ask yourself, _"what could possibly go wrong?"_. The answer is almost everything:
* People fall ill
* Laptops can be lost
* Files get overwritten
* Git merges can include unwanted code
* Requirements can be interpreted wrongly

## What to do
* Identify each risk, then for each risk identified:
* What is the probability that it may occur (H/M/L)
* What is the impact on the project should it occur (H/M/L)
* For each risk where Impact = H and probability is either H or M you will need devise a plan to reduce that impact



