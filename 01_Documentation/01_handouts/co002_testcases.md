# Handout - Testcases

## Design considerations
Some items that needs to be considered when writing testcases:
* State general assumptions E.g.:
  * _"the tests are performed by someone who is experienced in jump & run gaming"_
  * _"the tests are performed by someone who digitally literate"_
* State testing strategy:
  * _"Best weather testing"_ i.e only the best case conditions are tested
  * _"Test all possibilities & combinations"_ 

### The testcase should include
1. State any prerequistes such as:
   * any special test data that is needed
   * Any conditions that need to be fulfilled before starting the test
2. A reference to the Requirement (if applicable)
3. For each step that is to be performed:
   * Instructions what to do
   * The expected outcome
   * The actual outcome
3. Overall testcase result

## Tools 
### Excel (or similar)
* **Advantage**: simple to use and flexible
* **Disadvantage**: There is that risk that you end up with several different copies of the testcases. There is no history of the activities

### Jira/X-ray (or similar)
* **Advantage**: Central and has all the fields you need.
* **Disadvantage**: Sometimes it is not possible to customize the fields to suit your specific needs

### Examples
![Testcase for a game](../../01_Documentation/04_resources/images/01_co002_testcases_game.jpg)
![Testcase for a tasklist](../../01_Documentation/04_resources/images/01_co002_testcases_tasklist.jpg)

