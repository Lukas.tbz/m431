# Handout - requirements

There are two type of requirements: functional and non-functional

## Functional requirements
These relate directly to the system/application behaviour.  
Examples:
* The exit button must be red
* The the GUI must support both English & German
* Only that last 4 digits of the user's mobile number may be displayed

## Non-Functional requirements 
These relate to attributes that the system/application should fulfill.
* The response time should be no more than 100ms
* The browser must use HTTPS
* The application must be be backed up nightly

## Where do requirements come from?
* The end-user / project manager
* The company policy/strategy - _"we do not host hardware! We do everything in the cloud"_
* The government(s) regulation - _"Swiss customer data must remain in Switzerland"_
* Society 'Software should benefit mankind' 

## Where do you track requirements?
* In a list (Excel, or a requirements tool). 
* Make sure you document:
  * from where (or whom) this requirements comes from
  * When was this requirment raised
  * That the requirement has been reviewed by the person who raised it and is correct

