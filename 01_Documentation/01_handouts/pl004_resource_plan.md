# Handout - Resource Planning

## Introduction
* The resuorce plan is the simplest - but also the most useful plan; that you need as it allows you to see what days are available to perform assignment work or when critical resources (hardware) are available.

## How to do it
* Create a table
  * On the x-axis list each calander day until the assignment submission date
  * On the y-axis list:
    * Each team member who will work on the assignment
    * Each technical (hardware/software) resource that is needed (only if these are not standards PC's software)
  * For each resource ask:
    * For human resources ask when they are **not** available for work. Reasons for not available are typically, holiday, preparing for exams, work activities etc. Mark these days on your table.
    * For human resources ask when they are available for work. Mark these days on your table.
    * If it is a technical resource that is reqired then mark from when is is expected to be available  

## The result
* You now have an overview of the resources you need:
  * You see the time that is available to work on the assignment
  * You see periods where there may be no work performed
* Based on this you may need to ask team members to adjust their private plans or even adjust the scope of your assignment.
