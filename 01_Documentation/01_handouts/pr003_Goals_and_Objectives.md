# Handout - Goals

## Introduction
A good project (or task always has a goal). A good goal in turn should be realistic and properly defined. Let us look at _smart_ goals.

### SMART goal is:
* **S**pecific - It must be well defined and fit your task
* **M**easurable - you must be able to measure its progress (ideally in numbers) this implies that you must have a reference value  
* **A**chievable - this goal can be achieved within the given timeframe with the given means 
* **R**ealistic - It must relate to the task 
* **T**imely - It has a specific timeframe

### examples
* The goal of a new password reset self-service function could be: "_to reduce password reset calls to the servicedesk by 90% within 2 months of its launch_" 
* The goal of a new DevOps project to to fully automate deployments could be: "_to eliminate manual intervention in deploying applications completely._"
* The goal of a customer self-service portal could be: "_to reduce the service center staff my 5 PTE by then end of the year_"
* Example from real life [ex002]

### exercise
* Whihc goal(s) are SMART [exercise] 

[ex002]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/03_examples/pr002_Goals.md
[exercise]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/02_exercises/pr001_Goals.md
