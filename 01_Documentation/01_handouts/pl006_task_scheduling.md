# Handout - Task scheduling

## Introduction
* Now that you have a long list of tasks you will need to schedule them. The reason for doing this is:
  * To make a plan that shows what exactly gets performed by whom & when
  * To ensure that you can meet the assignment deadline

## How to do it
* Put the tasks into the order in which they can/should be performed
* For each task:
  * Associate each task with a phase of your assignment E.g. the Design phase
  * Estimate how long it may take (use units of half a day)
  * Assign the task to a team member 
* Create a table
  * On the x-axis list each calander day until the assignment submission date
  * On the y-axis you will have your task list.
* For each task:
  * As a team agree when this task can/should be performed
  * Consult the resource plan to ensure that the assignee is available to perform the work on the required date
  * Mark that date on your table.

## The result
* Now you will have a work plan. The work plan will show:
  * when all the tasks get preformed
  * You will be able to derrive when each phase (E.g. Design) will finish
  
## Next step
* Review the plan and ask yourselves the following questions: 
  * Will you finish the assignment on time?
  * Is the workload distributed evenly accross the team?
  * What will happen if a team member falls ill or is absent for a period of time?
* Adjust the plan, or the scope of your assignment if required

