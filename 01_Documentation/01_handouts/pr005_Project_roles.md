# Handout - Roles within a project

## Project Owner
This is typically a business unit which in turn is repersented by a person. 

## Stakeholder
Has a vested interest in success of the project. They may supply resources, products, money, people etc

## Project Manager
Is given the task realising the project's objective & goals

## 