# Handout - GIT

## What is Git
* Is a versioning system, it keeps track of all the versions of the files that you upload
* In the cloud you have typically github or gitlab

## What belongs in Git
* **Code** that you wrote (or use) for your Product (java, javascript, C-sharp, HTML, CSS etc)
* **Images** (jpegs/screenshots etc) that you create or use for either your Product or your documentation
* **SQL** scripts that you may have created when working with databases
* **Linux shell scripts/commands** that you may use to setup your environment
* basically any technical artifact that you either created or require for your Product & Assignment

## What does **not** belong in Git
* **Logfiles** - these have no added value
* **Passwords** 
* **Downloads** ideally you reference the place from where you downloaded them, if you need to keep the exact copy is should be stored elsewhere
* **Temporary files**

## How to interact with Git
* Integrated Development Environment (IDE)
  * VisualStudio
  * InteliJ
  * etc
* Command line
  * git bash

## Most common error
* Forgetting to **push** you changes

## Git lifecycle
![git lifecycle](../../01_Documentation/04_resources/images/01_re002_git_lifecycle.jpg)

## Git lifecycle with git bash
![git lifecycle](../../01_Documentation/04_resources/images/01_re002_git_bash_01.jpg)
![git lifecycle](../../01_Documentation/04_resources/images/01_re002_git_bash_02.jpg)
