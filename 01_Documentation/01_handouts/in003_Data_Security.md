# Handout - Data Security

## Introduction
In the past the company with the better hardware, the faster network, the better software had the cutting edge over its competitors. Today with cloud-computing and Software-as-a-Service (SaaS) it is easy to gain those extra resources as & when needed.

So, what is left that can really make the difference? The answer is Data and Employees.

Data is not just names & addresses it is also: configuration, logs, algorythms, events, VM's etc. In the right hands it is powerfull but in the wrong hands it can be toxic. As one of the companies main assets it must always be protected & handled with care.

Therefore, always ask yourself "_what's inside?_":
* Is it sensitive data?
* How must it be protected?
* Who should have access to it?
* What are the implications if the data is lost, corrupt or unusable?
* What happens if the data falls into the wrong hands?
* What are my duties & obligations with respect to this data?

### The role of the Data Owner
Just because you may look after a system (servers, DB's, code etc) that doesn't mean that the data processed by the application also belongs to you. It is not your job to decide who gets access to data, therefore always find out who the Data Owner is.
If in doubt ask the security officer  
