# Handout - 3 Constraints

## Definition
In project & assignment management there are 3 main constraints, namely:
* **Time**: the amount of time that you are given to perform your assignment.
* **Resources**: the items/skills at your disposal to perform the assignment such as: people, hardware, software etc.
* **Scope**: This describes the work that you plan to do, i.e. what is included.
Often one or more of these constraints is fixed and the others are variable. In our assignment time is fixed (static) as too are resources (people). Changing any of these has an impact on the others as shown in the diagram below.

![Diagram of constraints][constraints]

## Example of Scope
Describes (itemises) not only what you will do (in scope) but also what you plan not to do (out of scope).

### In scope
This describes what has to be done to complete a particular task/assignment
* Install development tools on PC
* Program function A
* Program function B
* Set up server
* Test functions A & B
* Document functions

### Out of scope
This is work that has to be done, but is not going to be included in the task/assignment 
* Apply new patches to PC
* Make App available in App Store 

[constraints]: ../../01_Documentation/04_resources/images/01_pr006_3_constraints.jpg
