# Handout - Roles & Responsibilities

## Role
This is similar to a title, "the system Architect", "Project Manager", "Developer"

## Resposibilities
These are the tasks that should be covered or performed by someone with this role 

### Example
* **Role:**
  * Database Administrator
* **Responsibilities:** 
  * Set up database according to company policy
  * Perform database tuning
  * Apply Oracle patches as required
  * Monitor database storage
  * Back-up database
