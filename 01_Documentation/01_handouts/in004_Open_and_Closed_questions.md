# Handout - Open & Closed questions

## Open question
An open question is desgined to get your subject talking, and to get them to describe what it is that they want. 
### Examples
1. _"What colour must it have?"_
2. _"What would be the ideal solution for you?"_
3. _"What features are essential to you?"_
4. _"What do you like about the current application?"_

## Closed question
A closed question will typically solicit a "_yes_" or "_no_" answer. It can be used if you want a straight answer, but if you are probing for information this can be be an inefficient process. 
### examples
1. "Is it blue?" - "No"
2. "Is it round?" - "Yes"

## A combintaion of Open & Closed
1. _"What colour should it have?"_ - _"blue would be ok"_
2. _"So, you want blue?_" - "yes"
