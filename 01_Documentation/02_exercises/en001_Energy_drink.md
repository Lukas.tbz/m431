# Exercise 

## Introduction
You are tired **and** thirsty, the only thing to do is to go to the kiosk and get an energy drink. The kiosk only has 2 energy drinks to chose from: the market leader _InstaBull_ and their own brand _slowNsteady_.

After careful study you gather your information:
* _InstaBull_ has a coffine content of 1g/100ml and costs 3.90 for a 200ml can. It is nice & chilled.
* _slowNsteady_ costs 3.50 for a 330ml can and has a coffine content of 3g/300ml, is not chilled but the price is reduced.
 
## Your task
Based on the information above and the fact that you are tired & thirsty create a simple decision matrix with 4 criteria to chose the right energy drink for you.
 