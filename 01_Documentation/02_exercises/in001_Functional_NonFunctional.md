# Exercise

## Introduction
Your customer plans to introduce a simple webtool. The user will enter the firstname, secondname, date of birth, AHV number and mobile number. After pressing _Submit_ they will get a SMS with a verification code.

**The requirements are as follows:**
1. All entry fields are marked as mandatory
2. The time taken between pressing _Submit_ and recieving the SMS should not be longer than 3 seconds
3. The system should be able to handle 100 concurrent sessions without a deterioation in performance
4. The user cannot submit a date of birth that is in the future
5. The data is to be held in the database for the next 10 years

## Your Task
Which of the requirements listed above is functional and whihc ones are non-functional
