# Exercise - New laptop

## Introduction
Disaster strikes and your laptop is broken beyond repair. Luckily you antcipated such an event and have reduced it's impact by arranging a nightly backup. Now your first priority is to get a new laptop by tomorrow latest! Your parents have kindly offered to pay between CHF 1400 - 1600 for the new laptop.

## Your task
* Design an evalauation matrix and apply an appropriate weighting to each criteria 
* Search the Internet to find a minimum of 3 laptops in the desired price range
* Use the evaluation matrix to select a new laptop that best fits your critera
