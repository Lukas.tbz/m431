# Exercise

## Introduction
Based on a recent true story, the names have been changed to protect the innocent. The project team were rolling out a new application. The application could run on either one of two servers (server-A or server-B). Alice (the project manager) calls Bob: "_I need you to write a simple shell-script to fail-over the application, simply stop the application on A and start it on B something like that. We will use it for the failover test in a few weeks._"

Bob immediatly goes to work:
* Bob reports on his progress each week and how tricky this script is
* Before running the script the code is reviewed by Alice
* Alice notices that the script doesn't failover at all, in fact the script only appears to stop & start the application on the same sever
* When confronted with this fact Bob's reply is: "_Yes, I know but I was told to name the script failover.sh I don't know why_" 
* Alice unit tests the script, it neither stopped nor started the application ...

## Your task
 1. When you consider the steps in IPERKA how could this have been avoided?  
 2. What questions should Bob have asked?




