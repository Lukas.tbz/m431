# Exercise - Create a resource plan

## Introduction
You need to create a resource plan for your assignment

## Your task
1. Together with you teammate(s) create a resource plan from today up until the end of the assignment.
2. Agree on the working hours (E.g. morning = 3 hours, Afternoon = 3 hours, Evening = 2 hours)
3. On you plan: 
    1. Identify the periods when you are available for work. Grey out the periods where you are not available
    2. Calculate the amount of hours that are available for the assignment per team member.
    3. Calculate the total amount of hours that are available for the assignment.

## Goal
* At the end of the exercise you will know:
  * How many hours are available
  * When you are available
  * When team members can work jointly on the assignment
* You then use this information to check that you can actually deliver the assigment in the intended scope with the hours available. 