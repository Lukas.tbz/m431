# Exercise - identify the tasks

## Introduction
You have been asked to implement a small part of a Flappy bird game using this [Tutorial]. 
* You have no tools currently installed on your PC.
* Remember the teacher's requirement concerning the use of JPEGs etc

## Your task
1. Read the tutorial carefully as far as - but not including, the section: "_Unity 2d flappy bird: infinite scrolling background_"
2. Identify what tools/software you will need in order to perform the job
3. Identify a minimum of 5 tasks that will need to be performed 
4. Put the tasks you identified into the correct sequence
5. If this is too easy for you, then you can also include the section "_Unity 2d flappy bird: infinite scrolling background_"

[Tutorial]: https://generalistprogrammer.com/unity/unity-2d-flappy-bird-how-to-make-flappy-bird/