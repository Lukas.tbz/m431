# Exercise

## Introduction
You are part of a project team rolling out a new interface for the HR application. There is a job that extracts user data from the HR database to a CSV file, that file is then sent to SAP where it will be imported. Alice - the project manager; calls you up on Teams: "_I need you to write a simple shell-script to copy the HR CSV file to the SAP system. We will need it for the testing next week ... Oh! I'm late for my next call if you need anything just let me know - bye!_"

## Your Task
Think of what questions you need to ask the project manager in order for you to start your work.

