# M431 - Assignments & Task Management

# Contents
## Project
### Project & Assignments:
 1. Definition of a process [pr001]
 2. Project lifecycle [pr002]
 3. Goals [pr003]
 4. Project roles [pr005]
 5. Roles & Responsibilities [pr004]
 6. 3 Constraints [pr006]

## IPERKA
### Inform
 * Information [in001]
 * Open & Closed questions [in004]
 * Inputs & Outputs [in001a]
 * Functional & non-functional requirements [in002]
 * Tipps about data [in003]
### Plan
 * Inputs & Outputs [pl001]

### Decide / (Entscheiden)
 * Inputs & Outputs [de001]

### Realisation / Implmentation
 * Inputs & Outputs [re001]

### Control
 * Inputs & Outputs [co001]

### Assess
 * Inputs & Outputs 

[pr001]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/pr001_Process.md
[pr002]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/pr002_Project_Lifecycle.md
[pr003]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/pr003_Goals_and_Objectives.md
[pr004]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/pr004_Roles_and_Responsibilities.md
[pr005]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/pr005_Project_roles.md
[pr006]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/pr006_3_constraints.md


[in001]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/in001_Information.md
[in002]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/in002_Requirements.md
[in003]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/in003_Data_Security.md
[in004]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/in004_Open_and_Closed_questions.md
[in001a]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/in001_Information_inputs_outputs.md

[pl001]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/pl001_Planning_inputs_outputs.md
[de001]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/de001_Decide_inputs_outputs.md
[re001]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/re001_Realisation_inputs_outputs.md
[co001]: https://gitlab.com/tbzh1/m431/-/blob/main/01_Documentation/01_handouts/co001_Control_inputs_outputs.md
