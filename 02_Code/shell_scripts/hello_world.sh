#! /bin/bash

SCRIPT_COMMENT="Hello world"
SCRIPT_MAX_NR=10

#
#
#
function sayHello {
   MY_COUNTER=0
   while MY_COUNTER<${SCRIPT_MAX_NR}; do
      echo "${SCRIPT_COMMENT} ${MY_COUNTER}"
	  MY_COUNTER=$(MY_COUNTER + 1)
   done
}

#
#
#
function main {
   sayHello
}

main

exit 0
